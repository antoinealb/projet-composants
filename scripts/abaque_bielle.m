close all;
clear all;

P = 3 % N
L = 0.5 % m

% acier
%E = 250e9 % Pa
% carbone
E = 120e9 % Pa

% Domaine des parametres 
D = logspace(-3, -2, 50);
e = logspace(-4, -3, 50);

% Moment quadratique 
I = @(D, e) pi/64 * (D.^4 - (D-2*e).^4);

% Rapport de deformation 
f = @(D, e) P * L^2 ./ (3 * E * I(D, e));

[xx, yy] = meshgrid(D, e);

colormap('autumn');
[C,h] = contour(1000*xx, 1000*yy, log(f(xx, yy)), -4:5);

text_handle = clabel(C,h);
set(text_handle,'BackgroundColor',[1 1 1.],...
    'Edgecolor',[.7 .7 .7])
set(h,'ShowText','on','TextStep',get(h,'LevelStep')*2)

grid on;
xlabel('Diametre e[mm]');
ylabel('Epaisseur [mm]');
title('log(W/L)');
